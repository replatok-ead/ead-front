import Vue from "vue";
import Router from "vue-router";
import Home from "@/views/Home.vue";
import HomeMvp from "../views/HomeMvp.vue";
import Login from "@/views/Login.vue";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "Home",
      redirect: "/home-mvp",
      component: Home,
    },
    {
      path: "/home-mvp",
      name: "HomeMvp",
      component: HomeMvp,
    },
    {
      path: "/login",
      name: "Login",
      component: Login,
    },
  ],
});
