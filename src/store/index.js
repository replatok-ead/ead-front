import Vue from "vue";
import Vuex from "vuex";
import actionUser from "./modules/actionUser.js";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    actionUser,
  },
});
