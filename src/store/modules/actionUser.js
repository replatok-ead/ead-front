import { api } from "../../services";

const registerUser = {
  namespaced: true,
  state: {
    user: {
      name: "",
      email: "",
      action: 0
    },
  },
  mutations: {
    SET_USER(state, payload) {
      state.user = Object.assign(state.user, payload);
    },
    SET_USER_ON_LOGIN(state, payload) {
      state.user = Object.assign(state.user, payload);
    },
  },
  actions: {
    registerUser(context, payload) {
      api
        .post("api/registeruser", payload)
        .then(() => {
          context.commit('SET_USER',{name: payload.name, email:payload.email, action:2})
          localStorage.setItem('user',JSON.stringify({name: payload.name, email:payload.email}))
        })
        .catch((error) => {
          console.trace(error.response);
          context.commit('SET_USER_ON_LOGIN',{action:3})
          setTimeout(()=>{
            context.commit('SET_USER_ON_LOGIN',{action:0})
          },50)
        });
    },
    setUserByLogged(context, payload){
      context.commit('SET_USER',{name: payload.name, email:payload.email})
    },
    loginUser(context, payload) {
      api
        .post("api/loginuser", payload)
        .then((response) => {
          context.commit('SET_USER_ON_LOGIN',{name: payload.name, email:response.data.user.email, action:1})
          localStorage.setItem('user',JSON.stringify({name: payload.name, email:response.data.user.email}))
        })
        .catch((error) => {
          console.trace(error.response);
          context.commit('SET_USER_ON_LOGIN',{action:3})
          setTimeout(()=>{
            context.commit('SET_USER_ON_LOGIN',{action:0})
          },50)
        });
    },
    logout(context){
      context.commit('SET_USER_ON_LOGIN',{name: "", email:"",action:0})
    }
  },
};

export default registerUser;
