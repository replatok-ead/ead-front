import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import Navbar from "@/components/Navbar.vue";

describe("Home.vue", () => {
  it("renders Logo", () => {
    const wrapper = shallowMount(Navbar);
    expect(wrapper.find("img"));
  });
});
